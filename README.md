Part of a little home/desktop project to light up a Coke bottle, this is a ESP32/NeoPixel app that provides a RESTful interface for controlling patterns of lights on the NeoPixel via JSON.

<center>
<img src="https://gitlab.com/kylemallory/bottleofcoke/raw/master/docs/20180929_122552.gif">
</center>

The 'Player' supports a Stack-style architecture, where multiple patterns can be queued, with the most recently pushed pattern at the top.
- The pattern on the top of the stack is the currently played pattern.
- If a new pattern is pushed while the previous pattern is being played, the previous pattern is preserved on the stack.
- As patterns complete, they are popped off the stack, and the previous pattern resumes.
- Patterns can be removed from the stack at any point (using the /pop endpoint).
- Patterns can be played indefinitely if the 'Count' parameter is set to '-1'. If the 'Count' parameter is set to 0, the pattern will not play and will be immediately popped back off the stack.
- If no patterns are left in the stack, the LED will turn off.


#### REST Endpoints
The following REST end points exist:

- POST:/set  causes the entire stack to be popped before adding the specified pattern.
- POST:/push causes the specified pattern to be pushed onto the top of the stack.
- GET:/pop causes the current/top pattern on the stack to be immediately popped. The pattern stops playing immediately, and the previous pattern resumes.
- GET:/status returns a json object from the top of the player stack indicating the uuid, name, and status (PLAYING or FINISHED) of that player.
- GET:/stack returns an array of json objects representing the entire stack. Each object indicated the uuid, name, and status (PLAYING or FINISHED) of that player.


```javascript
{
  'Name': 'Red-Green-Blue',
  'Count': 5, 
  'Pattern': [
    { 'Color': [255, 0, 0], 'Duration': 5000 },
    { 'Color': [0, 255, 0], 'Duration': 5000 },
    { 'Color': [0, 0, 255], 'Duration': 5000 }
  ]
}
```

#### TODO:
- ~~Add a 'transition' parameter to the Pattern elements, to allow animated/smooth transitions between colors (hold, linear, ease, etc)~~
- Naming a pattern saves it to flash storage; patterns can be played by simply specifying the name (ie, GET:/push?name=Red-Green-Blue)
- ~~Return unique ID's for pushed patterns - these can then be used to remove patterns from the stack by id (ie, GET:/pop?id=Red-Green-Blue-01FA22CD)~~
- ~~Add GET:/status to return the id/name of the current pattern, if any~~
- ~~Add GET:/stack to return an array of id/name that represents the current stack~~
- add GET:/remove?id={uuid} to remove players from the middle of the stack
- Add POST:/replace to pop the current top, and immediate push the new pattern (effectively replacing the existing pattern)
- Add a way to save the config over rest and save it
- Add an ap fallback

#### About/Construction
This was simple fun, weekend(ish) project for my kids. My step-daughter likes to make "slime". My oldest son loves coding, and my youngest son loves 3D-printing.
We decided to build a project where we could light up an 'empty' bottle, making it glow different colors, and tied into our home automation or other system to give us status information (for example, "Someone is playing on the Minecraft Server!", or "Internet is down!")
- My step-daughter (11yo) made a big batch of clear slime, complete with suspended glitter to make the bottle "glow" when lit up.
- Using the Sparkfun ESP Thing (ESP32), with an Adafruit NeoPixel, my oldest son (13yo) wrote most of the arduino code.
- My youngest son (10yo) and I will [eventually] design and buid the 3D printed base that contains the electronics and makes nice platform to hold the bottle.
- The bottle isn't attached, so it can be removed and replace with something else... Light up drink coaster? Holloween Candy Bowl? You name it...
- I project managed and helped with some of the technical challenges.
