#include "Config.h"

// Strip Setup

#include <NeoPixelBus.h>
#include <U8g2lib.h>

NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> strip(PixelCount, PixelPin);
U8X8_SSD1306_128X64_NONAME_SW_I2C u8g2(15, 4, 16);

void OLEDSetup() {
  u8g2.begin();
  u8g2.setFont(u8x8_font_chroma48medium8_r);
  u8g2.drawString(0, 0, "Display Configured...");
}

void StripSetup() {  
    strip.Begin();
    strip.Show();
}

// Wifi Setup

#include <WiFi.h>

// Replace with your network credentials

void WifiSetup() {
  
  WiFi.begin(ssid, password);
  WiFi.setHostname(hostname);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  Serial.println(WiFi.localIP());
  Serial.println("Wifi Setup Complete");
}

// MDNS Setup

#include <ESPmDNS.h>

void MDNSSetup() {
  if (!MDNS.begin(hostname)) {
        Serial.println("Error setting up MDNS responder!");
        while(1) {
            delay(1000);
        }
    }
  Serial.println("MDNS Setup Complete");
}

// OTA Setup

#include <ArduinoOTA.h>

void ArduinoOTASetup() {
  ArduinoOTA.setHostname(hostname);
  ArduinoOTA.setPassword(OTAPassword);
  ArduinoOTA.begin();
  Serial.println("OTA Setup Complete");
}

//Webserver Setup

#include <ESPmDNS.h>
#include <WebServer.h>

extern void ConfigureWebpages();

WebServer server(80);

void WebserverSetup() {
  
  ConfigureWebpages();
  server.begin();
  // Add server to MDNS
  MDNS.addService("http", "tcp", 80);
  Serial.println("Webserver Setup Complete");
}
