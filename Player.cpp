#include <ArduinoJson.h>
#include <NeoPixelBus.h>
#include <NeoPixelBrightnessBus.h>
#include <NeoPixelAnimator.h>
#include <ESP8266TrueRandom.h>

#define TRANSITION_HOLD 0
#define TRANSITION_LINEAR 1
#define TRANSITION_EASE 2

extern NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> strip;

class RGBD {
public:
      int red;
      int green;
      int blue;
      long duration;
      int transition;

      void setRGBD(int r, int g, int b, long d) {
        red = r, green = g, blue = b, duration = d;
      } 
};

class Player {
      String uuid;
      String patternName;
      int patternLength;
      RGBD *pattern;
      int count;

      bool debug = true;
      int stepIndex = -1;
      unsigned long stepExpire = -1;

      Player *parentPlayer = NULL;
      Player *childPlayer = NULL;

      RgbColor lastColor;
      RgbColor curColor;

public:
  
      Player(String JSON) {
        DynamicJsonBuffer jsonBuffer;

        doBlack(true);
        
        JsonObject& root = jsonBuffer.parseObject(JSON);
        if (!root.success()) {
          return;
        }

        byte uuidData[16];
        ESP8266TrueRandom.uuid(uuidData);
        uuid = ESP8266TrueRandom.uuidToString(uuidData);
        
        patternLength = root["Pattern"].size();
        count = root["Count"];
        patternName = root["Name"].as<String>();

        // loop through patternLenght and assign all the values;
        pattern = new RGBD[patternLength]();
        for(int i = 0; i < patternLength; i++) {
          pattern[i].red = root["Pattern"][i]["Color"][0];
          pattern[i].green = root["Pattern"][i]["Color"][1];
          pattern[i].blue = root["Pattern"][i]["Color"][2];
          pattern[i].duration = root["Pattern"][i]["Duration"];

          String transitionName = root["Pattern"][i]["Transition"].as<String>();
          if (transitionName.equalsIgnoreCase("linear")) pattern[i].transition = TRANSITION_LINEAR;
          else if (transitionName.equalsIgnoreCase("ease")) pattern[i].transition = TRANSITION_EASE;
          else pattern[i].transition = TRANSITION_HOLD;
        }
        
        stepIndex = -1;
        stepExpire = 0;
      }

      ~Player() {
        delete pattern;
      }

      JsonObject& getJSON() {
        DynamicJsonBuffer jsonBuffer;
        JsonObject& root = jsonBuffer.createObject();
        root["Name"] = patternName;
        root["Count"] = count;
        root["Id"] = uuid;

        // TODO: Finish this
      }

      bool isPlaying() {
        return ( count != 0);
      }

      void setParentPlayer(Player* player) {
        parentPlayer = player;
      }

      void setChildPlayer(Player* player) {
        Serial.println("Pushing '" + player->getPatternName() + "'.");
        childPlayer = player;
        player->parentPlayer = this;
      }

      void clearChildPlayer() {
        if (childPlayer != NULL) {
          childPlayer->clearChildPlayer();
          
          Serial.println("Popping '" + childPlayer->getPatternName() + "'.");
          delete childPlayer;
          childPlayer = NULL;
        }
      }

      Player* findYoungestPlayer() {
        Serial.print(".");
        if (childPlayer != NULL)
          return childPlayer->findYoungestPlayer();
        else
          return this;
      }
      
      Player* getParentPlayer() {
        return parentPlayer;
      }

      Player* getChildPlayer() {
        return childPlayer;
      }

      String getID() {
        return uuid;
      }

      String getPatternName() {
        return patternName;
      }

      boolean update() {
        if (childPlayer != NULL) {
          if (!childPlayer->update()) {
            clearChildPlayer();
          }
          return true;
        }
        return patternUpdate();
      }

      boolean patternUpdate() {
        long upTime = millis();

        if (count == 0) {
          return false;
        }
        
        if (debug) {
          static int cc = 0;
          cc++;
          if (cc > 1000) {
            cc = 0;
            Serial.print("[");
            Serial.print(patternName);
            Serial.print("] stepIndex:");
            Serial.print(stepIndex);
            Serial.print("  stepExpire:");
            Serial.print(stepExpire);
            Serial.print("  upTime:");
            Serial.print(upTime);
            Serial.print("  count:");
            Serial.println(count);
          }
        }

        // has the current step finished?
        if (stepExpire < upTime) {
          preserveColor(); // remember whatever our current color is
          stepIndex++; // start the next step

          // has the pattern finished?
          if ((stepIndex >= patternLength)) {
            if (count > 0) { // reduce count -- if count < 0 then we'll repeat forever
              count--;
            }
            // reset for the next pattern
            stepIndex = 0;
            stepExpire = 0;
          }
          stepExpire = millis() + pattern[stepIndex].duration;
        }
        
        // do color updates for the current step
        float duration = (float)(pattern[stepIndex].duration);
        float elapsed = (float)(duration - (stepExpire - millis()));
        float progress = elapsed / duration;
        
        switch (pattern[stepIndex].transition) {
          case TRANSITION_EASE:
            easeUpdate(progress);
            break;
          case TRANSITION_LINEAR:
            linearUpdate(progress);
            break;
          case TRANSITION_HOLD:
          default:
            holdUpdate(progress);
        }
        
        return true;
      }

      void holdUpdate(float progress) {
        doColor(pattern[stepIndex].red,pattern[stepIndex].green,pattern[stepIndex].blue, false);
      }

      boolean linearUpdate(float progress) {

        RgbColor outRGB(pattern[stepIndex].red, pattern[stepIndex].green, pattern[stepIndex].blue);
        RgbColor curRGB = RgbColor::LinearBlend(lastColor, outRGB, progress);

        doColor(curRGB, false);
      }

      boolean easeUpdate(float progress) {
        progress = 1.0 - ((cos(progress * 3.14159) / 2.0) + 0.5);
        
        RgbColor outRGB(pattern[stepIndex].red, pattern[stepIndex].green, pattern[stepIndex].blue);
        RgbColor curRGB = RgbColor::LinearBlend(lastColor, outRGB, progress);

        doColor(curRGB, false);
      }


      void preserveColor() {
        lastColor = curColor;
      }

      void doColor(RgbColor color, bool remember) {
        curColor = color;
        doColor(remember);
      }
      
      void doColor(RGBD color, bool remember) {
        doColor(color.red, color.green, color.blue, remember);
      }

      void doColor(int R, int G, int B, bool remember) {
        curColor.R = R;
        curColor.G = G;
        curColor.B = B;
        doColor(remember);
      }

      void doColor(bool remember) {
        strip.SetPixelColor(0,curColor);
        Serial2.write((const uint8_t *)&curColor, 3);
        strip.Show();
        
        if (remember)
          preserveColor();
      }

      void doBlack(bool remember) {
        doColor(0,0,0, remember);
      }
};

Player* myPlayer = NULL;

void PlayerUpdate() {
    if (myPlayer != NULL) {
      if (myPlayer->update() == false) {
        // if there is nothing to do, turn off the LED
        myPlayer->doBlack(true);
      }
    }
}

void PlayerSetPattern(String JSON) {
    if (myPlayer != NULL) {
      myPlayer->clearChildPlayer();
      delete myPlayer;
      myPlayer = NULL;
    }
    
    myPlayer = new Player(JSON);
    Serial.println("Setting '" + myPlayer->getPatternName() + "'");
}

void PlayerPushPattern(String JSON) {
  if (myPlayer != NULL) {
    Player* curPlayer = myPlayer->findYoungestPlayer();
    Serial.println();
    curPlayer->setChildPlayer(new Player(JSON));
  } else {
    PlayerSetPattern(JSON);
  }
}

void PlayerPopPattern() {
  if (myPlayer != NULL) {
    Player* curPlayer = myPlayer->findYoungestPlayer();
    Serial.println();
    if (curPlayer != NULL) {
      Player* parentPlayer = curPlayer->getParentPlayer();
      if (parentPlayer != NULL) {
        parentPlayer->clearChildPlayer();
      }
    }
  }
}

String PlayerDumpStack() {
  String result = "";
  DynamicJsonBuffer jsonBuffer;
  JsonArray& jsonArray = jsonBuffer.createArray();
  
  if (myPlayer != NULL) {
    Player* curPlayer = myPlayer;
    while (curPlayer != NULL) {
      JsonObject& jsonObject = curPlayer->getJSON();
      jsonArray.add(jsonObject);
      curPlayer = curPlayer->getChildPlayer();
    }
  }

  jsonArray.printTo(result);
  return result;
}

/**
 * Outputs as a String, an array of JSON Objects which represent the current Player Stack
 */
String PlayerGetStack() {
  String result = "";
  StaticJsonBuffer<512> jsonBuffer;
  JsonArray& jsonArray = jsonBuffer.createArray();
  
  if (myPlayer != NULL) {
    Player* curPlayer = myPlayer->findYoungestPlayer();
    while (curPlayer != NULL) {
      JsonObject& jsonObject = jsonBuffer.createObject();
      jsonObject["id"] = curPlayer->getID();
      jsonObject["name"] = curPlayer->getPatternName();
      jsonObject["status"] = curPlayer->isPlaying() ? "PLAYING: " : "FINISHED: ";
      curPlayer = curPlayer->getParentPlayer();
      jsonArray.add(jsonObject);
    }
  }

  jsonArray.printTo(result);
  return result;
}

String PlayerGetStatus() {
  String result = "";
  
  if (myPlayer != NULL) {
    StaticJsonBuffer<128> jsonBuffer;
    Player* curPlayer = myPlayer->findYoungestPlayer();
    JsonObject& jsonObject = jsonBuffer.createObject();
    jsonObject["id"] = curPlayer->getID();
    jsonObject["name"] = curPlayer->getPatternName();
    jsonObject["status"] = curPlayer->isPlaying() ? "PLAYING: " : "FINISHED: ";
    jsonObject.printTo(result);
  }
  
  return result;
}
