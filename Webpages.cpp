#include <WebServer.h>

extern WebServer server;
extern String PlayerDumpStack();
extern String PlayerGetStack();
extern String PlayerGetStatus();
extern void PlayerSetPattern(String JSON);
extern void PlayerPushPattern(String JSON);
extern void PlayerPopPattern();

String JSON="";

void webpageSet() {
 
  if (server.hasArg("plain")== false){ //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
 
  JSON = server.arg("plain");
  JSON += "\n";
  
  server.send(200, "application/json", JSON);
  
  PlayerSetPattern(JSON);
}

void webpagePush() {
 
  if (server.hasArg("plain")== false){ //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
 
  JSON = server.arg("plain");
  JSON += "\n";
  PlayerPushPattern(JSON);
  
  server.send(200, "application/json", PlayerGetStack());
}

void webpagePop() {
  server.send(200, "text/plain", "Top item Popped");
  PlayerPopPattern();
}

void webpageStatus() {
  server.send(200, "applicatino/json", PlayerGetStatus());
}

void webpageStack() {
  server.send(200, "application/json", PlayerGetStack());
}

void webpageDump() {
  server.send(200, "application/json", PlayerDumpStack());
}

void webpageRoot() {

  // Root Webpage content go here
  String content = "<!DOCTYPE HTML>\
  <html>\
      <body>\
        <p>Your looking for /set</p>\
      </body>\
  <html>";
  
  // Send Page
  server.send(200, "text/html", content);
  
}


void NotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += ( server.method() == HTTP_GET ) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";

  for ( uint8_t i = 0; i < server.args(); i++ ) {
    message += " " + server.argName ( i ) + ": " + server.arg ( i ) + "\n";
  }

  server.send ( 404, "text/plain", message );
  
}

void ConfigureWebpages() {

  //Configure Server URLS
  server.on("/", webpageRoot);
  server.on("/set", webpageSet);
  server.on("/push", webpagePush);
  server.on("/pop", webpagePop);
  server.on("/dump", webpageDump);
  server.on("/status", webpageStatus);
  server.on("/stack", webpageStack);
  server.onNotFound(NotFound);
}
