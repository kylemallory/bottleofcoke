#include <ArduinoOTA.h>
#include <WebServer.h>

extern void StripSetup();
extern void WifiSetup();
extern void MDNSSetup();
extern void WebserverSetup();
extern void ArduinoOTASetup();
extern void PlayerUpdate();

extern WebServer server;
// HardwareSerial Serial1(2);

void sendPixieRGB(byte r, byte g, byte b) {
  Serial2.write(r);
  Serial2.write(g);
  Serial2.write(b);
}

void setup() {
  // this resets all the neopixels to an off state
  Serial.begin(115200);
  Serial2.begin(115200);

  Serial.println("Seiral Port Works");
  Serial.println("Strip Setup");
  sendPixieRGB(32, 0, 0);
  StripSetup();
  delay(100);

  // Setup WiFi
  Serial.println("Wifi Setup");
  sendPixieRGB(32, 32, 0);
  WifiSetup();
  delay(100);

  // Setup MDNS Responder
  Serial.println("MDNS Setup");
  sendPixieRGB(0, 32, 0);
  MDNSSetup();
  delay(100);

  // Setup Websever
  Serial.println("Webserver Setup");
  sendPixieRGB( 0, 32, 32);
  WebserverSetup();
  delay(100);

  // Setup ArduinoOTA
  Serial.println("OTA Setup");
  sendPixieRGB( 0, 0, 32);
  ArduinoOTASetup();
  delay(100);

  sendPixieRGB( 0, 0, 0);
}

void loop() {
  //Serial.println("OTA HAndle");
  ArduinoOTA.handle();
  //Serial.println("Web Handle");
  server.handleClient();
  //Serial.println("Update Player");
  PlayerUpdate();



}
