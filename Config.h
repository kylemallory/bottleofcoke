#include <stdint.h>
// Replace with your network credentials
const char* ssid     = "your-wifi-ssid";
const char* password = "your-password-here";
// Put hostname for MDNS here
const char* hostname = "BottleOfCoke3W";

// Pin number for Leds
const uint8_t PixelPin = 13;
//Amount of leds
const uint16_t PixelCount = 1;

//OTA Update Passowrd
const char* OTAPassword = "admin-password";
